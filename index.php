<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Моя Виктория");
?>
	<!--meta name="viewport" content="width=1000"-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link href="css/style.css" rel="stylesheet" type="text/css" />
  <link href="css/media.css" rel="stylesheet" type="text/css" />
  <? require($_SERVER["DOCUMENT_ROOT"]."/adaptive-menu/index.php");?>
	<div class="center">
		<section class="callback-main">	
				<div class="container">	
					<div class="content">
						<p><span>Дорогой клиент или партнер,</span> спасибо, что решили поделиться с нами Вашим мнением или задать вопрос.</p>
						<p><span>Если Вы в магазине,</span> <br>то можете решить некоторые вопросы
						с администратором, на месте.</p>
						<a href="admins/" class="more"><span>Подробнее</span></a>
						<a href="http://victoria-group.ru/obratnaya-svyaz-1/feedback/" class="deal"><span>Оставьте обращение сейчас</span></a>
					</div>
				</div>
		</section>
		<section class="view">
			<div class="container">	
				<h2>Для нас важно Ваше мнение, поэтому:</h2>
				<div class="part">
					<div class="image"><img src="images/icon-1.png"></div>
					<p>Все обращения читает генеральный директор</p>
					</div>
				<div class="part">
					<div class="image"><img src="images/icon-2.png"></div>
					<p>Мы даем обратную связь в 100% случаев</p>
					</div>
				<div class="part">
					<div class="image"><img src="images/icon-3.png"></div>
					<p>Мы делаем выводы и учимся на своих ошибках</p>
					</div>
				<div class="part">
					<div class="image"><img src="images/icon-4.png"></div>
					<p>Наш сервис становится лучше</p>
					</div>
			</div>
		</section>
	</div>
	<!--script src="js/common.js"></script-->
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>