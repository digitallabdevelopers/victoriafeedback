﻿<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Моя Виктория");
?>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<!--meta name="viewport" content="width=1024"-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Администраторы</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script>
    if($(window).width() < 568){
        document.write('<link href="css/media.css" rel="stylesheet" type="text/css" /><link href="/adaptive-menu/css/style.css" rel="stylesheet" type="text/css" /><link href="/adaptive-menu/css/media.css" rel="stylesheet" type="text/css" />');
    }
</script>
<script src="../adaptive-menu/js/common.js"></script>
<link rel="stylesheet" type="text/css" href="../feedback/css/fa.css">
<? //require($_SERVER["DOCUMENT_ROOT"]."/adaptive-menu/index.php");?>
<div class="center">
    <section class="description">
        <p>Любая обратная связь для нас — это <br>возможность стать лучше для любимых <br>клиентов, поэтому мы всегда принимаем <br>ее с благодарностью.</p>
        <img src="images/logo.png">
    </section>
    <section class="pull">
        <h2>Мы собираем информацию и обратную связь от наших <br>клиентов из разных источников</h2>
        <div class="item">
            <span></span>
            <p>На сайте</p>
        </div>
        <div class="item">
            <span></span>
            <p>Через горячую линию</p>
        </div>
        <div class="item">
            <span></span>
            <p>Из книги жалоб</p>
        </div>
        <div class="item">
            <span></span>
            <p>Через наших сотрудников</p>
        </div>
    </section>
    <section class="steps">
        <h2>Затем обрабатываем каждую заявку за 4 шага</h2>
        <div class="item">
            <img src="images/step-1.png">
            <p>В зависимости от тематики, вашее обращение<br>попадает в соответствующий отдел  компании</p>
        </div>
        <div class="item">
            <img src="images/step-2.png">
            <p>Ответственные сотрудники тщательно <br>анализируют полученную информацию</p>
        </div>
        <div class="item">
            <img src="images/step-3.png">
            <p>Если ошибка была выявлена — мы принимаем<br>всевозможные меры, чтобы  ее устранить</p>
        </div>
        <div class="item">
            <img src="images/step-4.png">
            <p>Специалисты клиентской поддержки сообщают вам<br>результата обработки и устранения проблемы</p>
        </div>
    </section>
    <section class="answer">
        <p>Ежедневно ответственные сотрудники и генеральный директор встречаются для того, чтобы обсудить обратную связь от клиентов и принимают решение, как изменить сервис в магазинах, проверку продукции, качество обучения персонала и многое другое так, чтобы покупки приносили максимум удовольствия всем клиентам.</p>
        <p>И поскольку наша цель — доставлять счастье от покупок в наших супермаркетах, нам особенно приятно, когда клиенты говорят «Спасибо».</p>
    </section>
</div>
<!--script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script-->
<script src="js/common.js"></script>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>