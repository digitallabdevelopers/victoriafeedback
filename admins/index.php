﻿<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Моя Виктория");
?>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<!--meta name="viewport" content="width=1024"-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Администраторы</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script>
    if($(window).width() < 568){
        document.write('<link href="css/media.css" rel="stylesheet" type="text/css" /><link href="/adaptive-menu/css/style.css" rel="stylesheet" type="text/css" /><link href="/adaptive-menu/css/media.css" rel="stylesheet" type="text/css" />');
    }
</script>
<script src="../adaptive-menu/js/common.js"></script>
<link rel="stylesheet" type="text/css" href="../feedback/css/fa.css">
<? //require($_SERVER["DOCUMENT_ROOT"]."/adaptive-menu/index.php");?>
<div class="center">
    <section class="description">
        <p>Чтобы предоставить Вам качественный сервис <br>и решить проблему оперативно, в наших <br>магазинах — есть <span>администраторы</span>.</p>
        <img src="images/logo.png">
    </section>
    <section class="table">
        <h2>Администратор:</h2>
        <div class="item">
            <div class="l_el">
                <img src="images/item-1.png">
            </div>
            <p>Откроет дополнительную кассу <br>при наличии очереди</p>
        </div>
        <div class="item">
            <div class="l_el">
                <img src="images/item-2.png">
            </div>
            <p>Уточнит наличие товара в зале <br>или плановую дату поставки</p>
        </div>
        <div class="item">
            <div class="l_el">
                <img src="images/item-3.png">
            </div>
            <p>Поможет клиенту по вопросам возврата <br>и замены товара</p>
        </div>
        <div class="item">
            <div class="l_el">
                <img src="images/item-4.png">
            </div>
            <p>Поможет разобраться с качеством <br>приобретенного товара</p>
        </div>
        <div class="item">
            <div class="l_el">
                <img src="images/item-5.png">
            </div>
            <p>Решит вопросы чистоты и порядка <br>в торговом зале</p>
        </div>
        <div class="item">
            <div class="l_el">
                <img src="images/item-6.png">
            </div>
            <p>Проверит ценник на полке</p>
        </div>
        <div class="item">
            <div class="l_el">
                <img src="images/item-7.png">
            </div>
            <p>Проконсультирует по дополнитеным <br>услугам в зале: чистка рыбы, нарезка фарша
                <a href="/uslugi/">Все услуги<i class="fa fa-caret-right" aria-hidden="true"></i></a>
            </p>
        </div>
        <div class="item">
            <div class="l_el">
                <img src="images/item-8.png">
            </div>
            <p>Проверит корректность чека</p>
        </div>
    </section>
    <section class="facts">
        <h2>Некоторые факты и цифры</h2>
        <div class="item">
            <div class="count">20</div>
            <p>Каждый администратор проходит в год около <br>различных обучений итренингов.</p>
        </div>
        <div class="item">
            <div class="count">5</div>
            <p><span>лет необходимо отработать сотруднику в магазине, </span><br>чтобы стать администратором.</p>
        </div>
        <div class="item">
            <div class="count">1000</div>
            <p><span>На основе обратной связи от клиентов порядка</span> <br>процессов оптимизируются в компании ежемесячно, <br>начиная от приемки товара от поставщика и заканчивая <br>цветом чека для клиента.</p>
        </div>
        <div class="item">
            <div class="count">9000</div>
            <p><span>сотрудников компании «Виктория» ежедневно </span><br>думают как доставить улыбку и счастье своим клиентам.</p>
        </div>
        <div class="item">
            <div class="count">40000</div>
            <p><span>Еженедельно мы проверяем качество</span> <br>товаров, чтобы с любовью предоставить клиентам <br>только свежие продукты со всего света.</p>
        </div>
    </section>
    <section class="question">
        <h2>Остались вопросы?</h2>
        <a href="../feedback/"><span>Задать вопрос<i class="fa fa-caret-right" aria-hidden="true"></i></span></a>
    </section>
</div>
<!--script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script-->
<script src="js/common.js"></script>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>