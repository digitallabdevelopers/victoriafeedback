$(document).ready(function() {
	var imagePath, imageUrl;
  // Создание canvas
  html2canvas((function() {
    var haiku = $('.haiku-appled');
    haiku.append(
      $('<img>').prop({
        'src' : 'images/apple.png',
        'width' : 65,
        'height' : 65,
        'class' : 'apple'
      })
    );

    return haiku;
  })(), {
    onrendered: function(canvas) {
      document.body.appendChild(canvas);
      makeIT();
    }
  });
	var url = location.href;
  url = url.replace(location.search, '');
	// Конверт в .png
	function makeIT(){
    var canvas = $('canvas')[0];
    var data = canvas.toDataURL('image/png').replace(/data:image\/png;base64,/, '');
    $('canvas').remove();
    $.post('saveCPic.php',{data:data}, function(rep){
      //alert('Изображение ' + rep + ' сохранено' );
      imagePath = rep;
      imageUrl = url + imagePath;
    });
  }
  // Удаление конверта
  function delIT(){
    $.post('delCPIc.php', {image: imagePath}, function(rep){
      //alert('Изображение удалено');
    });
  }
  window.onbeforeunload = delIT();
  //$(window).unload(function() {
  // delIT();
  //});
  $('.social a').click(function(e){
    e.preventDefault();
  })
  // Шаринг VK
  $(document).on('click', '#btn-vk', $.proxy(function() {
    var i = location.protocol + "//" + location.host + '/polovinki';
    var r = "#моявиктория";
    var n = "http://vkontakte.ru/share.php?url=" + encodeURIComponent(i) + "&title=" + encodeURIComponent(r) + "&image=" + encodeURIComponent(imageUrl) + "&noparse=false&t=" + (new Date().getTime());
    var newWin = window.open(n, "", "toolbar=0,status=0,width=626,height=436")
		newWin.onunload = function () {
      location.reload();
    };
    	newWin.focus();
    }, this));
   // Шаринг Facebook
   	$(document).on('click', '#btn-fb', $.proxy(function() {
        FB.ui({
          'method': 'feed',
          'picture' : imageUrl,
          'caption' : 'www.victoria-group.ru',
          'name' : 'Пожелание от Виктории',
          'hashtag' : '#моявиктория',
          'display' : 'touch',
          'link' : location.host + '/polovinki'
        },function (response) {
          if (response && !response.error) {
            location.reload();
          }
        });
      }, this));
    window.fbAsyncInit = function() {
    FB.init({
      appId      : '164376097391119',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

});