<?
if($_SERVER['SERVER_NAME'] !== 'victoria.local') {
  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
  $APPLICATION->SetTitle("Обратная связь");
}
?>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!--meta name="viewport" content="width=1024" /-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Обратная связь</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <? if($_SERVER['SERVER_NAME'] == 'victoria.local') { ?>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    <? } ?>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/media.css">
    <link rel="stylesheet" type="text/css" href="../feedback/css/fa.css">
    <script>
    //if($(window).width() < 568){
        //document.write('<link href="css/media.css" rel="stylesheet" type="text/css" /><link href="/adaptive-menu/css/style.css" rel="stylesheet" type="text/css" /><link href="/adaptive-menu/css/media.css" rel="stylesheet" type="text/css" />');
    //}
    </script>
    <? if($_SERVER['SERVER_NAME'] !== 'victoria.local') {
        require($_SERVER["DOCUMENT_ROOT"]."/adaptive-menu/index.php");
    }?>
    <div class="center">
        <section class="content">
           <h2>Ваше обращение принято</h2>
           <p>Узнайте, <a href="../order/">как мы будем обрабатывать вашу заявку <i class="sep fa fa-caret-right"></i></a></p>
           <p>или прочитайте хайку, которую мы подготовили специально для Вас.</p>
           <div class="haiku">
               <? require('switch.php');?>
               <img src="images/haiku-<? echo $_GET['id'];?>.png">
           </div>
            <div class="haiku-appled">
              <? require('switch.php');?>
                <img src="images/haiku-<? echo $_GET['id'];?>.png">
            </div>
           <div class="social">
               <p>Поделитесь хайку:</p>
               <a href="" id="btn-vk"><img src="images/vk.png"></a>
               <a href="" id="btn-fb"><img src="images/fb.png"></a>
           </div>
        </section>
    </div>
    <!--script src="https://code.jquery.com/jquery-1.8.0.min.js" integrity="sha256-jFdOCgY5bfpwZLi0YODkqNXQdIxKpm6y5O/fy0baSzE=" crossorigin="anonymous"></script-->
    <script type="text/javascript" src="js/html2canvas.min.js"></script>
    <script type="text/javascript" src="js/common.js"></script>
<? if($_SERVER['SERVER_NAME'] !== 'victoria.local') {
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
    } ?>