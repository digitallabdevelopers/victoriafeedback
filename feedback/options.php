<?php
  require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
  CModule::IncludeModule("iblock");

  function getListFromInfoblock($iblockId) {
    $res = CIBlockElement::GetList(array("sort"=>"asc", "name"=>"asc"), array("IBLOCK_ID"=> $iblockId));
    $list = array();
    while ($el = $res->GetNext()) {
      // Проперть с магазинами
      $list[(string) $el['ID']] = $el['NAME'];
    }

    return $list;
  }

	$options = array(
	  'products' => getListFromInfoblock(37), // Товары по стикерам
    'shops' => getListFromInfoblock(21) // Список магазинов
   );

  echo json_encode($options);
?>