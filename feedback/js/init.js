window.VC =
{

init : function()
{
    $('body').delegate('.cmd', 'click', function(e){

        e.preventDefault();
        e.stopImmediatePropagation();

        var $t = $(this);
        if(!$t.is('.inack'))
        {
            var m = $t.data('method').split('.');
            window[m[0]][m[1]]($t);
        }

        return false;
    });

    $('select.autoSB').sb();
},

search : function($d)
{
    $d.parents('form')[0].submit();
},

submenu : function($d)
{
    $ul = $d.closest('li').eq(0).find('>ul').stop();
    if($d.hasClass('active'))
    {
        $d.removeClass('active');
        $ul.hide('slow');
    }
    else
    {
        $d.addClass('active');
        $ul.show('slow');
    }
},


/* j5yykkPcw
name : function()
{

},

*/

nop:false

}

$(document).ready(VC.init);
function coorIcons() {
    if($("input").is(".check")){
        var positionCheck = $("input.check").parent('.fa-container').position().top;
        var heightCheck = $("input.check").parent('.fa-container').outerHeight();
        heightCheck = heightCheck / 2;
        var coordinateCheck = positionCheck + heightCheck;
        $('input.check').before('<i class="fa fa-info check"></i>');
        var imageCheckHeight = $('img.check').height();
        imageCheckHeight = imageCheckHeight / 2;
        var coordinateImgCheck = coordinateCheck - imageCheckHeight;
        $('img.check').css('top', coordinateImgCheck + 'px');
        $(".fa-info.check").hover(function(){ $( "img.check" ).fadeIn();},function(){$( "img.check" ).hide();});
    }
    if($("input").is(".card")){
        var positionCard = $("input.card").parent('.fa-container').position().top;
        var heightCard = $("input.card").parent('.fa-container').outerHeight();
        heightCard = heightCard / 2;
        var coordinateCard = positionCard + heightCard;
        $('input.card').before('<i class="fa fa-info card"></i>');
        var imageCardHeight = $('img.check').height();
        imageCardHeight = imageCardHeight / 2;
        var coordinateImgCard = coordinateCard - imageCardHeight;
        $('img.card').css('top', coordinateImgCard + 'px');
        $(".fa-info.card").hover(function(){ $( "img.card" ).fadeIn();},function(){$( "img.card" ).hide();});
    }
}
$(document).ready(function(){
    $(document).on('click', '.select p',  function(){
        if (!$(this).parent('.select').data('toggled') || $(this).parent('.select').data('toggled') == 'off'){
            $(this).parent('.select').siblings('.select').children('.scroll').hide();
            $(this).parent('.select').siblings('.select').children('p').children('i').addClass('fa-caret-down');
            $(this).parent('.select').siblings('.select').children('p').children('i').removeClass('fa-caret-up');
            $(this).parent('.select').children('.scroll').show(); 
            $(this).parent('.select').children('p').children('i').removeClass('fa-caret-down');
            $(this).parent('.select').children('p').children('i').addClass('fa-caret-up');
            $(this).parent('.select').data('toggled','on');
        }else if ($(this).parent('.select').data('toggled') == 'on'){
            $(this).parent('.select').children('.scroll').hide();
            $(this).parent('.select').children('p').children('i').addClass('fa-caret-down');
            $(this).parent('.select').children('p').children('i').removeClass('fa-caret-up');
            $(this).parent('.select').data('toggled','off');
        }
    });
    $(document).on('click', '.select .scroll input',  function(){
        $(this).parent('.scroll').show();
        $(this).parent('.scroll').parent('.select').data('toggled','on');
    });
    $(document).on('click', '.scroll ul li',  function(){
        $(this).parent('ul').parent('.scroll').siblings('p').children('span').html($(this).children('p').html());
        $(this).parent('ul').parent('.scroll').siblings('p').children('span').css("color","#000");
        $(this).parent('ul').parent('.scroll').parent('.select').css('border-color','#d9d9d6');
        var className = $(this).parent('ul').parent('.scroll').parent('.select').attr('class');
        className = className.replace('select ', '');
        $("#" + className + " option:selected").prop("selected", false);
        $("#" + className + " [value='" + $(this).data('value') + "']").prop("selected", true);
        $(this).parent('ul').parent('.scroll').parent('.select').data('use', 1);
        $(this).parent('ul').parent('.scroll').hide();
        $(this).parent('ul').parent('.scroll').parent('.select').data('toggled','off');
        $(this).parent('ul').parent('.scroll').parent('.select').children('p').children('i').addClass('fa-caret-down');
        $(this).parent('ul').parent('.scroll').parent('.select').children('p').children('i').removeClass('fa-caret-up');
        $(this).parent('ul').parent('.scroll').parent('.select').siblings('#error-' + className).remove();
        $(this).parent('ul').parent('.scroll').parent('.select').css('border-color','#c4d600');
        //console.log($("#worker").val());
    });
    $('label.checkbox').click(function(){
        if (!$(this).data('actived') || $(this).data('actived') == 'off'){
            $('span.checkbox').css('background-image', 'url("../form/images/checkbox-active.png")');
            $('span.checkbox').data('actived','on');
            $('#i-agree').prop('checked', false);
            $(this).data('actived','on');
        }else if ($(this).data('actived') == 'on'){
            $('span.checkbox').css('background-image', 'url("../form/images/checkbox.png")');
            $('span.checkbox').data('actived','off');
            $('#i-agree').prop('checked', true);
            $(this).data('actived','off');
        }
    });


    if($('select').is('#products') || $('select').is('#shops')){
        $.ajax({
            url: "../options.php",
            type: "POST",
            success: function(html){
                //var newOptions = JSON.parse(html);
                var newOptions = newOptions != "" ? $.parseJSON(html) : {};
                if($('select').is('#products')){
                    $('#products').after('<div class="select products" data-toggled="off" data-use="0"><p><span>Товар</span><i class="fa fa-caret-down" aria-hidden="true"></i></p><div id="product-list" class="scroll"><input type="text" placeholder="Поиск" class="fuzzy-search"/><ul class="list"></ul></div></div>'); 
                    $.each(newOptions['products'], function(key, value) {
                        $('#products').append('<option value="' + key +'">' + value + '</option>');
                        $('.products div ul').append('<li data-value="' + key +'"><p class="product">' + value + '</p></li>');
                    });
                    var shopList = new List('product-list', { 
                        valueNames: ['product']
                    });
                }
                if($('select').is('#shops')){
                    $('#shops').after('<div class="select shops" data-toggled="off" data-use="0"><p><span>Магазин</span><i class="fa fa-caret-down" aria-hidden="true"></i></p><div id="shop-list" class="scroll"><input type="text" placeholder="Поиск" class="fuzzy-search"/><ul class="list"></ul></div></div>');
                    $.each(newOptions['shops'], function(key, value) {
                        $('#shops').append('<option value="' + key +'">' + value + '</option>');
                        $('.shops div ul').append('<li data-value="' + key +'"><p class="shop">' + value + '</p></li>');
                    });
                    var shopList = new List('shop-list', { 
                        valueNames: ['shop']
                    }); 
                }
                coorIcons();
            }
        });
    }else{
        coorIcons();
    }
    if($('select').is('#worker')){
        $('#worker').after('<div class="select worker" data-toggled="off" data-use="0"><p><span>Сотрудник</span><i class="fa fa-caret-down" aria-hidden="true"></i></p><div class="scroll"><ul></ul></div></div>');
        $('#worker option').each(function(){
            $('.worker .scroll ul').append('<li data-value="' + $(this).val() +'"><p>' + $(this).text() + '</p></li>')
        });
    }
    $('menu.feedback a.sub1').click(function(){
        if(!$(this).hasClass('active')){
            $(this).css('border','0');
            $(this).siblings('span').fadeOut();
        }else{
            $(this).css('border-bottom','1px dotted');
            $(this).siblings('span').fadeIn();
        }
    });
    var ta = document.querySelector('textarea');
    ta.addEventListener('focus', function(){
      autosize(ta);
    });
    $('input[name="card"]').click(function(){
        $(this).focus();
    });
    $('input[name="contact"]').click(function(){
        $(this).focus();
    });
    $("input.card").mask("26 99999999999", {});
    $("input.phone").mask("+7 (999) 999-99-99", {});
    var val = 0;
    if($('input').is('#file')){
        $('input#file').before("<div class='fa-container'><i class='fa fa-paperclip' aria-hidden='true'></i><input type='text' name='files' id='file-mask' disabled='disabled' placeholder='Файл'></input></div>")
    }
    var countForm = 0;
    function onStep(){
        if(!$('div').is('.select')){
            if($('fieldset.step-1 input.required:blank').length == 0){
                $('.step-1').hide();
                $('.step-2').show('slow');   
                $('.crumbs a').removeClass('active');
                $('.crumbs a:last-child').addClass('active');
                countForm++;
            }
        }else if($('div').is('.select')){
            var selectLength = $('fieldset.step-1 .select').length;
            var selectUse = 0;
            $('fieldset.step-1 .select').each(function(){
                if($(this).data('use') == 1){
                    //$(this).css('border-color','#c4d600');
                    selectUse++;
                }else{
                    $(this).css('border-color','#D22630');
                    var errorId = $(this).attr('class');
                    errorId = errorId.replace('select ', '');
                    $(this).after('<label class="error" id="error-' + errorId + '">Это поле обязательно для заполнения <i class="fa fa-exclamation-triangle" aria-hidden="true"></i></label>')
                }
            });
            if (selectUse == selectLength && $('fieldset.step-1 input.required:blank').length == 0){
                $('.step-1').hide();
                $('.step-2').show('slow');   
                $('.crumbs a').removeClass('active');
                $('.crumbs a:last-child').addClass('active');
                countForm++;
            }
        }   
    }
    $(document).on('mouseenter', 'fieldset.step-1',  function(){
        if(!$('div').is('.select')){
            if($('fieldset.step-1 input.required:blank').length == 0){
                $('.on').css('color','#c4d600');
            }else{
                $('.on').css('color','#878787');
            }
        }else if($('div').is('.select')){
            var selectLength = $('fieldset.step-1 .select').length;
            var selectUse = 0;
            $('fieldset.step-1 .select').each(function(){
                if($(this).data('use') == 1){
                    selectUse++;
                }
            });
            if (selectUse == selectLength && $('fieldset.step-1 input.required:blank').length == 0){
                $('.controls .on').css('color','#c4d600');
            }else{
                $('.controls .on').css('color','#878787');
            }
        }
    });
    $(document).on('mouseenter', 'center',  function(){
        if($('fieldset.step-2 input.required:blank').length == 0){
            $('.controls  button').css('color','#c4d600');
        }else{
            $('.controls  button').css('color','#878787');
        }
    
    });
    $(document).on('click', '.controls .on',  function(){
        form.valid();
        onStep();
    });
    $(document).on('click', '.right a',  function(e){
        var step = $(this).attr('id');
        if(step == 'step-3'){
            form.valid();
            onStep();
        }else if(step == 'step-2'){
            $('.step-2').hide();
            $('.step-1').show('slow');
            $('.crumbs a').removeClass('active');
            $('.crumbs a:nth-of-type(2)').addClass('active');
            countForm--;
        }else if(step == 'step-1'){
            var link = url('path');
            link = link.replace("/form","");
            console.log(link);
            window.location.href = link;
        }
        e.preventDefault();
    });
    $(document).on('click', 'i.fa-paperclip',  function(){
        $('#file').click();
        return(false);
    });
    $('#file').change(function(){
        val = $(this).val();
        $('#file-mask').val(val.substring(val.lastIndexOf('\\')+1,val.length).replace(/\.[^.]+$/, ""));
    });
    $('.controls .back').click(function(e){
        if(countForm > 0){
            $('.step-2').hide();
            $('.step-1').show('slow');
            $('.crumbs a').removeClass('active');
            $('.crumbs a:nth-of-type(2)').addClass('active');
            countForm--;
        }else{
            var link = url('path');
            link = link.replace("/form","");
            //console.log(link);
            window.location.href = link;
        }
        e.preventDefault(); 
    });
    $('textarea').val('');
    var form = $('#form');
    var validator = form.validate({
        success: "valid",
        rules:{
            check:{
                required: true,
                number: true,
            },
            name:{
                required: true,
                cyrilicFormat: true,
                minlength: 2,
                maxlength: 50,
            },
            phone:{
                required: true,
            },
            email:{
                email:true,
            },
        },
        messages:{
            check:{
                required: "Это поле обязательно для заполнения <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>",
                number: "Используйте только цифры <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>",
            },
            phone:{
               required: "Это поле обязательно для заполнения", 
            },
            name:{
                required: "Это поле обязательно для заполнения <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>",
                minlength: "Минимальное количество символов - 2 <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>",
                maxlength: "Максимальное количество символов - 50 <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>"
            },
            email:{
                email: "Проверьте правильность вашего электронного адреса <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>",
            }
        },
    });
    $.validator.addMethod("cyrilicFormat",
      function (value, element) {
          return value.match(/(^[а-я]{1}[а-я]{1,14}$)|(^[а-я]{1}[а-я]{1,14} [а-я]{1}[а-я]{1,14}$)|(^[А-Я]{1}[а-я]{1,14}$)|(^[А-Я]{1}[а-я]{1,14} [А-Я]{1}[а-я]{1,14}$)/);
      }, "Нельзя использовать латиницу, цифры и спецсимволы. <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>");
});