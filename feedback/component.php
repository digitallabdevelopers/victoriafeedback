<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (CModule::IncludeModule("iblock"))
{
	
	if(empty($arParams["IBLOCK_ID"])) {
		ShowError(GetMessage("UFORM_IBLOCK_NOT_FOUND"));
		return;
	}
	
	if(!is_numeric($arParams["IBLOCK_ID"])) {
		$rsIBlock = CIBlock::GetList(array(), array(
			"ACTIVE" => "Y",
			"CODE" => $arParams["IBLOCK_ID"],
		));
		if ($arIBlock = $rsIBlock->GetNext()) {
			$arParams["IBLOCK_ID"] = $arIBlock["ID"];
		}
	}

	if($arParams["IBLOCK_ID"] > 0)
		$bWorkflowIncluded = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "WORKFLOW") == "Y" && CModule::IncludeModule("workflow");
	else
		$bWorkflowIncluded = CModule::IncludeModule("workflow");

	$arParams["ID"] = intval($_REQUEST["CODE"]);

	$showAllProperties = false;
	if(empty($arParams["PROPERTY_CODES"])) {
		$arParams["PROPERTY_CODES"] = array();
		$showAllProperties = true;
	}
	else {
		foreach($arParams["PROPERTY_CODES"] as $i=>$k)
			if(strlen($k) <= 0)
				unset($arParams["PROPERTY_CODES"][$i]);
	}
	
	$arParams["USER_MESSAGE_ADD"] = trim($arParams["USER_MESSAGE_ADD"]);
	if(strlen($arParams["USER_MESSAGE_ADD"]) <= 0)
		$arParams["USER_MESSAGE_ADD"] = GetMessage("IBLOCK_USER_MESSAGE_ADD_DEFAULT");

	if(!is_array($arParams["GROUPS"]))
		$arParams["GROUPS"] = array();

	$arGroups = $USER->GetUserGroupArray();

	if (empty($arParams["GROUPS"])) {
		$bAllowAccess = true;
	}
	elseif ($arParams["ID"] == 0) {
		$bAllowAccess = count(array_intersect($arGroups, $arParams["GROUPS"])) > 0 || $USER->IsAdmin();
	}
	else {
		$bAllowAccess = $USER->GetID() > 0;
	}

	$arResult["ERRORS"] = array();
	
	$iblock = GetIBlock($arParams['IBLOCK_ID']);
	$arResult['NAME'] = $iblock['NAME'];

	if ($bAllowAccess) {
		// get iblock sections list
		$rsIBlockSectionList = CIBlockSection::GetList(
			array("left_margin"=>"asc"),
			array(
				"ACTIVE"=>"Y",
				"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
			),
			false,
			array("ID", "NAME", "DEPTH_LEVEL")
		);
		$arResult["SECTION_LIST"] = array();
		while ($arSection = $rsIBlockSectionList->GetNext()) {
			$arSection["NAME"] = str_repeat(" . ", $arSection["DEPTH_LEVEL"]).$arSection["NAME"];
			$arResult["SECTION_LIST"][$arSection["ID"]] = array(
				"VALUE" => $arSection["NAME"]
			);
		}

		$arResult["PROPERTY_LIST"] = array();
		
		//преобразовываем символьные коды свойств в id
		foreach ($arParams["PROPERTY_CODES"] as $key => $propertyID) {
			if ((!is_int($propertyID))) {
				$res = CIBlockProperty::GetList(Array(), Array("CODE" => $propertyID, "IBLOCK_ID" => $arParams['IBLOCK_ID']));
				if ($ar_res = $res->GetNext()) $arParams["PROPERTY_CODES"][$key] = $ar_res['ID'];
			}
		}

		// get iblock property list
		$rsIBLockPropertyList = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"]));
		while ($arProperty = $rsIBLockPropertyList->GetNext()) {
			// get list of property enum values
			if ($arProperty["PROPERTY_TYPE"] == "L")
			{
				$rsPropertyEnum = CIBlockProperty::GetPropertyEnum($arProperty["ID"]);
				$arProperty["ENUM"] = array();
				while ($arPropertyEnum = $rsPropertyEnum->GetNext())
				{
					$arProperty["ENUM"][$arPropertyEnum["ID"]] = $arPropertyEnum;
				}
			}

			if ($arProperty["PROPERTY_TYPE"] == "T") {
				if (empty($arProperty["COL_COUNT"])) $arProperty["COL_COUNT"] = "30";
				if (empty($arProperty["ROW_COUNT"])) $arProperty["ROW_COUNT"] = "5";
			}

			if(strlen($arProperty["USER_TYPE"]) > 0 ) {
				$arUserType = CIBlockProperty::GetUserType($arProperty["USER_TYPE"]);
				if(array_key_exists("GetPublicEditHTML", $arUserType))
					$arProperty["GetPublicEditHTML"] = $arUserType["GetPublicEditHTML"];
				else
					$arProperty["GetPublicEditHTML"] = false;
			}
			else {
				$arProperty["GetPublicEditHTML"] = false;
			}

			// add property to edit-list
			if ($showAllProperties || in_array($arProperty["ID"], $arParams["PROPERTY_CODES"]))
				$arResult["PROPERTY_LIST"][] = $arProperty["ID"];

			$arResult["PROPERTY_LIST_FULL"][$arProperty["ID"]] = $arProperty;

			$arResult["PROPERTY_IDS"][$arProperty["CODE"]] = $arProperty["ID"];
		}

		// set starting filter value
		$arFilter = array("IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"], "IBLOCK_ID" => $arParams["IBLOCK_ID"], "SHOW_NEW" => "Y");

		//check for access to current element
		if ($arParams["ID"] > 0) {
			if (empty($arFilter["ID"])) $arFilter["ID"] = $arParams["ID"];

			// get current iblock element

			$rsIBlockElements = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter);

			if ($arElement = $rsIBlockElements->Fetch()) {
				$bAllowAccess = true;

				if ($bWorkflowIncluded) {
					$LAST_ID = CIBlockElement::WF_GetLast($arElement['ID']);
					if ($LAST_ID != $arElement["ID"]) {
						$rsElement = CIBlockElement::GetByID($LAST_ID);
						$arElement = $rsElement->Fetch();
					}

					if (!in_array($arElement["WF_STATUS_ID"], $arParams["STATUS"])) {
						echo ShowError(GetMessage("IBLOCK_ADD_ACCESS_DENIED"));
						$bAllowAccess = false;
					}
				}
				else {
					if (in_array("INACTIVE", $arParams["STATUS"]) === true && $arElement["ACTIVE"] !== "N") {
						echo ShowError(GetMessage("IBLOCK_ADD_ACCESS_DENIED"));
						$bAllowAccess = false;
					}
				}
			}
			else {
				echo ShowError(GetMessage("IBLOCK_ADD_ELEMENT_NOT_FOUND"));
				$bAllowAccess = false;
			}
		}
	}

	if ($bAllowAccess) {

		//prepare data for form

		if ($arParams["ID"] > 0) {
			// $arElement is defined before in elements rights check
			$rsElementSections = CIBlockElement::GetElementGroups($arElement["ID"]);
			$arElement["IBLOCK_SECTION"] = array();
			while ($arSection = $rsElementSections->GetNext()) {
				$arElement["IBLOCK_SECTION"][] = array("VALUE" => $arSection["ID"]);
			}

			$arResult["ELEMENT"] = array();
			foreach($arElement as $key => $value) {
				$arResult["ELEMENT"]["~".$key] = $value;
				if(!is_array($value) && !is_object($value))
					$arResult["ELEMENT"][$key] = htmlspecialchars($value);
				else
					$arResult["ELEMENT"][$key] = $value;
			}


			// load element properties
			$rsElementProperties = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arElement["ID"], $by="sort", $order="asc");
			$arResult["ELEMENT_PROPERTIES"] = array();
			while ($arElementProperty = $rsElementProperties->Fetch()) {
				if(!array_key_exists($arElementProperty["ID"], $arResult["ELEMENT_PROPERTIES"]))
					$arResult["ELEMENT_PROPERTIES"][$arElementProperty["ID"]] = array();

				if(is_array($arElementProperty["VALUE"])) {
					$htmlvalue = array();
					foreach($arElementProperty["VALUE"] as $k => $v)
					{
						if(is_array($v))
						{
							$htmlvalue[$k] = array();
							foreach($v as $k1 => $v1)
								$htmlvalue[$k][$k1] = htmlspecialchars($v1);
						}
						else
						{
							$htmlvalue[$k] = htmlspecialchars($v);
						}
					}
				}
				else {
					$htmlvalue = htmlspecialchars($arElementProperty["VALUE"]);
				}

				$arResult["ELEMENT_PROPERTIES"][$arElementProperty["ID"]][] = array(
					"ID" => htmlspecialchars($arElementProperty["ID"]),
					"VALUE" => $htmlvalue,
					"~VALUE" => $arElementProperty["VALUE"],
					"VALUE_ID" => htmlspecialchars($arElementProperty["PROPERTY_VALUE_ID"]),
					"VALUE_ENUM" => htmlspecialchars($arElementProperty["VALUE_ENUM"]),
				);
			}

			// process element property files
			$arResult["ELEMENT_FILES"] = array();
			foreach ($arResult["PROPERTY_LIST"] as $propertyID) {
			
				$arProperty = $arResult["PROPERTY_LIST_FULL"][$propertyID];
				if ($arProperty["PROPERTY_TYPE"] == "F") {
					$arValues = array();
					if (intval($propertyID) > 0) {
						foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arProperty)
						{
							$arValues[] = $arProperty["VALUE"];
						}
					}
					else {
						$arValues[] = $arResult["ELEMENT"][$propertyID];
					}

					foreach ($arValues as $value) {
						if ($arFile = CFile::GetFileArray($value)) {
							$arFile["IS_IMAGE"] = CFile::IsImage($arFile["FILE_NAME"], $arFile["CONTENT_TYPE"]);
							$arResult["ELEMENT_FILES"][$value] = $arFile;
						}
					}
				}
			}

			$bShowForm = true;
		}
		else {
			$bShowForm = true;
		}

		if ($bShowForm) {
			// prepare form data if some errors occured
			if (count($arResult["ERRORS"]) > 0) {
				foreach ($arUpdateValues as $key => $value) {
					if ($key == "IBLOCK_SECTION") {
						$arResult["ELEMENT"][$key] = array();
						if(!is_array($value)) {
							$arResult["ELEMENT"][$key][] = array("VALUE" => htmlspecialchars($value));
						}
						else {
							foreach ($value as $vkey => $vvalue) {
								$arResult["ELEMENT"][$key][$vkey] = array("VALUE" => htmlspecialchars($vvalue));
							}
						}
					}
					elseif ($key == "PROPERTY_VALUES") {
						//Skip
					}
					elseif ($arResult["PROPERTY_LIST_FULL"][$key]["PROPERTY_TYPE"] == "F") {
						//Skip
					}
					elseif ($arResult["PROPERTY_LIST_FULL"][$key]["PROPERTY_TYPE"] == "HTML") {
						$arResult["ELEMENT"][$key] = $value;
					}
					else
					{
						$arResult["ELEMENT"][$key] = htmlspecialchars($value);
					}
				}

				foreach ($arUpdatePropertyValues as $key => $value) {
					if ($arResult["PROPERTY_LIST_FULL"][$key]["PROPERTY_TYPE"] != "F") {
						$arResult["ELEMENT_PROPERTIES"][$key] = array();
						if(!is_array($value)) {
							$value = array(
								array("VALUE" => $value),
							);
						}
						foreach($value as $vv) {
							if(is_array($vv)) {
								if(array_key_exists("VALUE", $vv))
									$arResult["ELEMENT_PROPERTIES"][$key][] = array(
										"~VALUE" => $vv["VALUE"],
										"VALUE" => htmlspecialchars($vv["VALUE"]),
									);
								else
									$arResult["ELEMENT_PROPERTIES"][$key][] = array(
										"~VALUE" => $vv,
										"VALUE" => $vv,
									);
							}
							else {
								$arResult["ELEMENT_PROPERTIES"][$key][] = array(
									"~VALUE" => $vv,
									"VALUE" => htmlspecialchars($vv),
								);
							}
						}
					}
				}
			}
			// генерируем captcha
			$arResult['CAPTCHA'] = false;
			if ($arParams['CAPTCHA'] == 'Y' || ($arParams['CAPTCHA'] == 'UNAUTH' && !$USER->IsAuthorized())) {
				$arResult['CAPTCHA_CODE'] = $APPLICATION->CaptchaGetCode();
			}
			
			if ($arParams['POPUP'] != 'Y') {
				//определяем место нахождения файла, где подключается компонент
				$aTrace = debug_backtrace();
				foreach($aTrace as $trace) {
					if ($trace['function'] == 'IncludeComponent' && $trace['class'] == 'CAllMain') {
						$sSrcLine = $trace['line'];
						$sSrcFile = str_replace(array('\\', $_SERVER['DOCUMENT_ROOT']), array('/', ''), $trace['file']);
						break;
					}
				}
				$arParams['SRC_PATH'] = $sSrcFile;
				$arParams['SRC_LINE'] = $sSrcLine;
			}

			$this->IncludeComponentTemplate();
		}
	}
	if (!$bAllowAccess && !$bHideAuth)
	{
		$APPLICATION->AuthForm("");
	}
}

?>