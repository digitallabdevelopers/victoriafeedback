<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=1024" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Обратная связь</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/feedback.css">
    <link rel="stylesheet" type="text/css" href="css/ui.css">
    <link rel="stylesheet" type="text/css" href="css/fa.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jq.sb.js"></script>
    <script type="text/javascript" src="js/init.js"></script>
</head>
<body>

<header class="main">
    <div class="left">
        <a href="/">
            <img src="css/images/logo.png">
        </a>
    </div>
    <div class="right">
        <div class="fContainer info">
            <div class="fItem fFix search">
                <div class="inner">
                    <form method="GET" action="/search.php">
                    <input class="search" type="text" name="s" value="" placeholder="Поиск">
                    <span class="cmd fa fa-search" data-method="VC.search"></span>
                    </form>
                </div>
            </div>
            <div class="fItem fMore contact">
                <div>
                    <select class="autoSB citySelect">
                        <option value="0" selected="selected">Москва и область</option>
                        <option value="1">Париж (2500км. от МКАД)</option>
                    </select>
                </div>
                <div>
                    <a class="phone" href="tel:88002004454">8 800 200 44 54</a>
                </div>
                <div class="">
                    <span class="fa fa-envelope-o"></span>
                </div>
            </div>
        </div>
        <nav>
            <ul class="fContainer">
                <li><a href="#">О компании</a></li>
                <li><a href="#">Акции</a></li>
                <li><a href="#">Моя Виктория</a></li>
                <li><a href="#">Рецепты</a></li>
                <li><a href="#">Партнерам</a></li>
                <li><a class="animated flip" href="feedback.php">Обратная связь</a></li>
            </ul>
        </nav>
    </div>
</header>


<div class="main">
    <h1>Поиск: <i><?=htmlspecialchars($_GET['s']);?></i>
</div>

</body>
</html>
