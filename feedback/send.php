<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");


$iblockId = 38; // Обращения с сайта и на горячую линию (с мороженкой)
$iblockProps = array();
define(SITE_ID, 's1');

/**
 * Проверка существования почтового шаблона
 * @param $template
 */
function prepareTemplate($template)
{
  $res = CEventMessage::GetList($by = "site_id", $order = "desc", array('TYPE_ID' => $template));
  if (!$ar_res = $res->GetNext()) {
    $arFields = array(
      'LID' => SITE_ID,
      'EVENT_NAME' => $template,
      'NAME' => GetMessage("UFORM_FORMS"),
      'DESCRIPTION' => ''
    );
    $et = new CEventType;
    $et->Add($arFields);

    $arFields = array(
      'ACTIVE' => 'Y',
      'LID' => SITE_ID,
      'EVENT_NAME' => $template,
      'EMAIL_FROM' => '#MAIL_SEND#',
      'EMAIL_TO' => '#MAIL_SEND#',
      'SUBJECT' => '#SUBJECT#',
      'BODY_TYPE' => 'html',
      'MESSAGE' => '#TEXT#',
    );
    $emess = new CEventMessage;
    $emess->Add($arFields);
  }
}

/**
 * Получение пропертей для инфоблока
 */

function getProperties($props, $iblockId)
{
  // Существование формы в принципе
  if (!isset($props['formId']) || (int)$props['formId'] < 1 || (int)$props['formId'] > 32) {
    return false;
  }

  $arProp = array(
    'id' => array(
      'fieldname' => 'ELEMENT_ID',
      'label' => 'Заявка номер ',
      'template' => '<h1>{label} {content}</h1>'
    ),
    // Мск или КГД
    'region' => array(
      'fieldname' => 'PROPERTY_REGION',
      'required' => array(),
      'label' => 'Регион:  '
    ),
    // Магазин
    'shop' => array(
      'fieldname' => 'PROPERTY_SHOP',
      'required' => array(1, 2, 3, 5, 10, 11, 12, 13, 14, 15, 25, 26, 27, 28, 30),
      'label' => 'Магазин: '
    ),
    // Товар по стикерам?
    'products' => array(
      'fieldname' => 'PROPERTY_STICKER_TRADE',
      'required' => array(2),
      'label' => 'Товар по стикеру: '
    ),
    // Коммент
    'comment' => array(
      'fieldname' => 'PROPERTY_QUESTION',
      'required' => array(),
      'label' => 'Подробное описание: '
    ),
    // Номер карты
    'card' => array(
      'fieldname' => 'PROPERTY_NUMBER',
      'required' => array(),
      'label' => 'Номер карты: '
    ),
    // Номер чека
    'check' => array(
      'fieldname' => 'PROPERTY_CHEQUE',
      'required' => array(3, 4, 5, 6, 7, 9, 12, 13),
      'label' => 'Номер чека: '
    ),
    // Товар
    'product' => array(
      'fieldname' => 'PROPERTY_TRADE',
      'required' => array(),
      'label' => 'Товар: '
    ),
    // Телефон
    'contact' => array(
      'fieldname' => 'PROPERTY_PHONE',
      'required' => array(),
      'label' => 'Контактный телефон: '
    ),
    // Адрес эл. почты
    'email' => array(
      'fieldname' => 'PROPERTY_EMAIL',
      'required' => array(),
      'label' => 'От кого: '
    ),
    // Тип сотрудника
    'worker' => array(
      'fieldname' => 'PROPERTY_EMPLOYEE_TYPE',
      'label' => 'Тип сотрудника: '
    ),
    // ФИО сотрудника
    'employee' => array(
      'fieldname' => 'PROPERTY_EMPLOYEE',
      'required' => array(10),
      'label' => 'ФИО сотрудника: '
    ),
    // Имя человека, который пишет
    'name' => array(
      'fieldname' => 'PROPERTY_NAME',
      'label' => 'Имя: '
    ),
    // Тип обращения
    'type' => array(
      'fieldname' => 'PROPERTY_TYPE',
      'label' => 'Тип обращения: ',
      'template' => '<h3>{label} {content}</h3>'
    ),
    // Запросил клиент обратную связь или нет
    'needFeedback' => array(
      'fieldname' => 'PROPERTY_FEEDBACK_NEED',
      'label' => 'Клиент запросил обратную связь: '
    ),
    // Рейтинг
    'emotions' => array(
      'fieldname' => 'PROPERTY_RATING',
      'label' => 'Оценка: '
    ),
    // Файл
    'file' => array(
      'fieldname' => 'PROPERTY_FILE',
      'label' => 'Файл: '
    ),
    // Процесс
    'process' => array(
      'fieldname' => 'PROCESS',
      'label' => 'Процесс: '
    ),
    // Тематика
    'subprocess' => array(
      'fieldname' => 'SUBPROCESS',
      'label' => 'Тематика обращения: '
    ),
    // Тема
    'theme' => array(
      'fieldname' => 'THEME',
      'label' => 'Тема: '
    ),
    // Подтема
    'subtheme' => array(
      'fieldname' => 'SUBTHEME',
      'label' => 'Подтема: '
    ),
    // Горячая линия или нет
    'gl' => array(
      'fieldname' => 'PROPERTY_GL',
      'label' => 'Оформлено на GL: '
    )
  );

  /**
   * Проверка на пустоту и обязательность
   */
  $resProps = array();
  foreach ($arProp as $key => $params) {
    if (isset($params['required']) && array_search($props['formId'], $params['required']) !== false && (!isset($props[$key]) || empty($props[$key]))) {
      return false;
    }

    if(isset($props[$key])) {
      if(isset($params['template'])) {
        $str = str_replace('{label}', $params['label'], $params['template']);
        $str = str_replace('{content}', $props[$key], $str);
        $resProps[$params['fieldname']] = $str;
      } else {
        $resProps[$params['fieldname']] = '<p>'.$params['label'].$props[$key].'</p>';
      }

      // Чистые поля - это для формы инфоблока в админке
      $resProps[$params['fieldname'].'_CLEAN'] = $props[$key];
    } else {
      $resProps[$params['fieldname']] = '';
    }
  }

  return $resProps;
}


/**
 * Генерация элемента типа блока (запись об обратной связи)
 * @param $iblockId
 * @param $iblockProps
 * @param $text
 */
function generateIBlockElement($iblockId, $iblockProps, $template)
{
  global $USER, $APPLICATION;

  $el = new CIBlockElement;

  $arLoadProductArray = Array(
    "MODIFIED_BY"    => $USER->GetID(),
    "IBLOCK_ID"      => $iblockId,
    "PROPERTY_VALUES"=> $iblockProps,
    "NAME"           => date('d.m.Y H:i:s'),
    "PREVIEW_TEXT"   => '',
    "ACTIVE"         => "N"
  );

  $id = $el->Add($arLoadProductArray);
  return $id;
}

/**
 * Выбрать тип обращения
 * @param $menu
 * @param array $path
 * @param array $data
 * @return array
 */
function forms($menu, $path = array(), $data = array()){
  foreach($menu->item as $sub)
  {
    $name = $path;
    $name[] = $sub->text;

    if($sub->fieldset)
      $data[] = array(
        implode(" - ", $name),
        $sub->fieldset
      );
    elseif($sub->menu)
      $data = forms($sub->menu, $name, $data);
  }

  return $data;
}

$region = getRegionCode();
// Существование рейтинга в форме
if (!isset($_POST['formId']) || (int)$_POST['formId'] < 1 || (int)$_POST['formId'] > 32) {
  $result = array(
    'success' => false
  );
} else {
  $postProps = $_POST;
  //if(!isset($postProps['control'])){
  //  echo "Ошибка. Ваша заявка распознана как спам. Попробуйте снова.";
  //  header('Location: /obratnaya-svyaz-1/feedback/form/?id='.$postProps['formId']);
  //}
  // Выбрать темплейт
  if($region == 'moskva-i-oblast') {
    $mail_template = 'FEEDBACK_MOSCOW_NEW';
    $postProps['region'] = 203;
  } elseif ($_POST['region'] == 1) { // Калининград
    $mail_template = 'FEEDBACK_KENIG_NEW';
    $postProps['region'] = 204;
  }

  // Тип обращения вычислим по форме
  $res = CIBlockElement::GetList(array("sort" => "asc", "name" => "asc"), array("PROPERTY_FORM_ID" => $postProps['formId']));
  $el = $res->GetNext();
  $parts = array_map(function($element) {
    return trim($element);
  }, explode('-', $el['NAME']));

  // Процесс .... подтема
  if(isset($parts[0])) {
    $postProps['process'] = $parts[0];
  }
  if(isset($parts[1])) {
    $postProps['subprocess'] = $parts[1];
  }
  if(isset($parts[2])) {
    $postProps['theme'] = $parts[2];
  }
  if(isset($parts[3])) {
    $postProps['subtheme'] = $parts[3];
  }

  $postProps['type'] = $el['ID'];

  // Горячая линия или нет
  if(!isset($postProps['gl'])) {
    $postProps['gl'] = 209;
  }

  /**
   * F-g shit.
   */
  if(!isset($postProps['needFeedback'])) {
    $postProps['needFeedback'] = 205; // Не нужна связь
  } else {
    $postProps['needFeedback'] = 206; // Нужна связь
  }

  // Если есть файл, который загружают, подгрузить..
  if(!empty($_FILES)) {
    $arFile = array(
      "name" => $_FILES['file']['name'],
      "size" => $_FILES['file']['size'],
      "tmp_name" => $_FILES['file']['tmp_name'],
      "type" => $_FILES['file']['type'],
      "old_file" => "",
      "del" => "N",
      "MODULE_ID" => "iblock",
      "description" => "Загруженный файл"
    );
    $fileId = CFile::SaveFile($arFile, '.');
    if ($arFile = CFile::GetFileArray($fileId)) {
      $postProps['file'] = $arFile;
    }
  }

  // Получим проперти
  $iblockProps = getProperties($postProps, $iblockId);
  // Подготовим шаблон
  prepareTemplate($mail_template);
  // Запишем в базу
  $id = generateIBlockElement($iblockId, $iblockProps, $mail_template);
  $postProps['id'] = $id;

  /**
   * Готовим для отправки данные..
   */
  // Обратная связь...
  if($postProps['needFeedback'] == 205) {
    $postProps['needFeedback'] = 'нет';
  } else {
    $postProps['needFeedback'] = 'да';
  }

  // Регион...
  if($postProps['region'] == 203) {
    $postProps['region'] = 'Москва и область';
  }
  if($postProps['region'] == 204) {
    $postProps['region'] = 'Калининград';
  }

  // Товар по стикерам: теперь его можно в строковом представлении
  if(isset($postProps['products'])) {
    $res = CIBlockElement::GetList(array("sort" => "asc", "name" => "asc"), array("ID" => $postProps['products']));
    $el = $res->GetNext();
    $postProps['products'] = $el['NAME'];
  }

  // Магазин: его тоже можно в строку
  if(isset($postProps['shop'])) {
    $res = CIBlockElement::GetList(array("sort" => "asc", "name" => "asc"), array("ID" => $postProps['shop']));
    $el = $res->GetNext();
    $postProps['shop'] = $el['CODE'].' '.$el['NAME'];
  }

  // Офомлено на горячей линии или нет, строковое
  if(isset($postProps['gl'])) {
    if($postProps['gl'] == 208) {
      $postProps['gl'] = 'да';
    } else {
      $postProps['gl'] = 'нет';
    }
  }


  if(isset($postProps['file'])) {
    $postProps['file'] = '<img src="http://victoria-group.ru'.$postProps['file']['SRC'].'" alt="" />';
  }

  // Оценка
  if(isset($postProps['emotions'])) {
    $emotions = array(
      197 => '1 - Ужасно',
      198 => '2 - Плохо',
      199 => '3 - Удовлетворительно',
      200 => '4 - Нормально',
      201 => '5 - Хорошо',
      202 => '6 - Отлично'
    );
    $postProps['emotions'] = $emotions[$postProps['emotions']];
  }

  // Тип обращения вычислим по форме, только теперь возьмём строковое представление
  $res = CIBlockElement::GetList(array("sort" => "asc", "name" => "asc"), array("PROPERTY_FORM_ID" => $postProps['formId']));
  $el = $res->GetNext();
  $postProps['type'] = $el['NAME'];

  // Перегенерируем для заявки, вместе с ID
  $iblockProps = getProperties($postProps, $iblockId);
  if(empty($postProps['control'])){
    // Отошлём почту
    CEvent::Send($mail_template, SITE_ID, $iblockProps);
    header('Location: /obratnaya-svyaz-1/thanks/?id='.$postProps['formId'].'&name='.$postProps['name']);
  }else{
    header('Location: /obratnaya-svyaz-1/feedback/form/?id='.$postProps['formId']);
  }
}

?>