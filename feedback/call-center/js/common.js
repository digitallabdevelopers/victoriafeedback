$(document).ready(function(){

});
$(function(){
	$.ajax({
    url: "options.php",
    type: "POST",
    success: function(html){
        var newOptions = newOptions != "" ? $.parseJSON(html) : {};
        if($('select').is('#region')){
          $.each(newOptions['region'], function(key, value) {
              $('#region').append('<option value="' + key +'">' + value + '</option>');
              $('.region div ul').append('<li data-value="' + key +'"><p class="region">' + value + '</p></li>');
          });
        }
        if($('select').is('#shop')){
           $.each(newOptions['shop'], function(key, value) {
               $('#shop').append('<option value="' + key +'">' + value + '</option>');
               $('.shop div ul').append('<li data-value="' + key +'"><p class="shop">' + value + '</p></li>');
           });
           var shopList = new List('shop-list', { 
               valueNames: ['shop']
           }); 
        }
        if($('select').is('#type')){
           $.each(newOptions['type'], function(key, value) {
               $('#type').append('<option value="' + key +'">' + value + '</option>');
               $('.type div ul').append('<li data-value="' + key +'"><p class="type">' + value + '</p></li>');
           });
           var shopList = new List('type-list', { 
               valueNames: ['type']
           }); 
        }
    }
  });
  $('.time span').click(function(){
    if ($(this).data('on') == '0'){
  	 $(this).children('ul').show();
     $(this).data('on', 1);
    }else if($(this).data('on') == '1'){
      $(this).children('ul').hide();
      $(this).data('on', 0);
    }
  });
  $('.time span li').click(function(){
    var name = $(this).parent('span').attr('class');
    $('#' + name).val($(this).text());
    $(this).parent('ul').siblings('b').text($(this).text());
    $(this).parent('ul').siblings('b').css('color','#000');
  });
  $('label.contact').click(function(){
    if ($(this).children('#contact').prop('checked') == false){
      $(this).children('span').css('background-image', 'url("../call-center/images/checkbox-active.png")');
      $(this).children('p').css('color','#000');
      $('.time').show();
      $(this).children('#contact').prop('checked', true);
    }else if ($(this).children('#contact').prop('checked') == true){
      $(this).children('span').css('background-image', 'url("../call-center/images/checkbox.png")');
      $(this).children('p').css('color','#b1b1b1');
      $('.time').hide();
      $(this).children('#contact').prop('checked', false);
    }
  });
  $(document).on('change', '#file', function(){
    var val = $(this).val();
    console.log(val);
    $('#file-mask').val(val.substring(val.lastIndexOf('\\')+1,val.length).replace(/\.[^.]+$/, ""));
  });
  $(document).on('click', '.select p',  function(){
    if ($(this).parent('.select').data('toggled') == 'off'){
      $(this).parent('.select').siblings('.select').children('.scroll').hide();
      $(this).parent('.select').siblings('.select').children('p').children('i').addClass('fa-caret-down');
      $(this).parent('.select').siblings('.select').children('p').children('i').removeClass('fa-caret-up');
      $(this).parent('.select').children('.scroll').show(); 
      $(this).parent('.select').children('p').children('i').removeClass('fa-caret-down');
      $(this).parent('.select').children('p').children('i').addClass('fa-caret-up');
      $(this).parent('.select').data('toggled','on');
    }else if ($(this).parent('.select').data('toggled') == 'on'){
      $(this).parent('.select').children('.scroll').hide();
      $(this).parent('.select').children('p').children('i').addClass('fa-caret-down');
      $(this).parent('.select').children('p').children('i').removeClass('fa-caret-up');
      $(this).parent('.select').data('toggled','off');
    }
   });
  $(document).on('click', '.scroll ul li',  function(){
    $(this).parent('ul').parent('.scroll').siblings('p').children('span').html($(this).children('p').html());
    $(this).parent('ul').parent('.scroll').siblings('p').children('span').css("color","#000");
    //$(this).parent('ul').parent('.scroll').parent('.select').css('border-color','#d9d9d6');
    var className = $(this).parent('ul').parent('.scroll').parent('.select').attr('class');
    className = className.replace('select ', '');
    $("#" + className + " option:selected").prop("selected", false);
    $("#" + className + " [value='" + $(this).data('value') + "']").prop("selected", true);
    $(this).parent('ul').parent('.scroll').parent('.select').data('use', 1);
    $(this).parent('ul').parent('.scroll').hide();
    $(this).parent('ul').parent('.scroll').parent('.select').data('toggled','off');
    $(this).parent('ul').parent('.scroll').parent('.select').children('p').children('i').addClass('fa-caret-down');
    $(this).parent('ul').parent('.scroll').parent('.select').children('p').children('i').removeClass('fa-caret-up');
    $(this).parent('ul').parent('.scroll').parent('.select').siblings('#error-' + className).remove();
    //$(this).parent('ul').parent('.scroll').parent('.select').css('border-color','#c4d600');
  });
  $(document).on('click', '.type ul li',  function(){
  	var count = $(this).data('value');
  	$('.subtype div ul li').remove();
  	$('.subtype p span').text('Уточнить тип обращения').css('color', '#a9a9a9');
  	$('.subtype').hide();
  	$.ajax({
    	url: "types.php",
    	type: "POST",
    	data: {count: count},
    	success: function(html){
    		if(html){
       		var newTypes = newTypes != "" ? $.parseJSON(html) : {};
       		$.each(newTypes, function(key, value) {
        	    $('#subtype').append('<option value="' + key +'">' + value + '</option>');
        	    $('.subtype div ul').append('<li data-value="' + key +'"><p class="type">' + value + '</p></li>');
        	 });
       		$('.subtype').show();
       	}
       }
 		});
  });
  var ta = document.querySelector('textarea');
  ta.addEventListener('focus', function(){
    autosize(ta);
  });
})