<?
    if($_SERVER['SERVER_NAME'] !== 'victoria.local') {
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
        $APPLICATION->SetTitle("Обратная связь");
    }
?>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=1024" />
    <!--meta name="viewport" content="width=device-width, initial-scale=1"-->
    <title>Обратная связь</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--link rel="stylesheet" type="text/css" href="css/media.css"-->
    <link rel="stylesheet" type="text/css" href="css/fa.css">
    <div class="center">
        <section class="feedback">
            <form action="../../send.php" method="POST">
                <input type="hidden" name='control'>
                <input type="hidden" name='gl' value="208">
                <input type="text" name='name' class="required" placeholder="Имя и Отчество*">
                <input type="text" class='phone' name='contact' placeholder="Телефон*">
                <input type="text" name='email' placeholder="Электронный адрес">
                <div class="select region" data-toggled="off" data-use="0">
                    <p>
                        <span>Регион*</span>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </p>
                    <div id="region-list" class="scroll">
                        <ul class="list"></ul>
                    </div>
                </div>
                <div class="select shop" data-toggled="off" data-use="0">
                    <p>
                        <span>Магазин</span>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </p>
                    <div id="shop-list" class="scroll">
                        <input type="text" placeholder="Поиск" class="fuzzy-search"/>
                        <ul class="list"></ul>
                    </div>
                </div>
                <div class="select type" data-toggled="off" data-use="0">
                    <p>
                        <span>Тип обращения</span>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </p>
                    <div id="type-list" class="scroll">
                            <input type="text" placeholder="Поиск" class="fuzzy-search"/>
                            <ul class="list"></ul>
                    </div>
                </div>
                <div class="select subtype" data-toggled="off" data-use="0">
                    <p>
                        <span>Уточнить тип обращения</span>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </p>
                    <div id="shop-list" class="scroll">
                        <input type="text" placeholder="Поиск" class="fuzzy-search"/>
                        <ul class="list"></ul>
                    </div>
                </div>
                <select name='region' id="region"></select>
                <select name="shop" id="shop"></select>
                <select name="type" id="type"></select>
                <select name="subtype" id="subtype"></select>
                <textarea placeholder='Комментарий' name='comment'></textarea>
                <label for="file" class="forFile">
                    <i class='fa fa-paperclip' aria-hidden='true'></i>
                    <input type='text' name='file-mask' id='file-mask' disabled='disabled' placeholder='Файл'/>
                </label>
                <input type='file' id='file' name='file' placeholder='Прикрепить файл'> 
                <label class="contact" data-actived="off" for="contact">
                    <input type="checkbox" id="contact" name="needFeedback">
                    <span class="checkbox" name="needFeedback"></span>  
                    <p>Cвязаться после обработки обращения</p>  
                </label>
                <div class="time">
                    <select name="after" id="after">
                        <? for ($i=9; $i < 21; $i++) {?>
                            <option value="<? echo $i; ?>:00"><? echo $i; ?>:00</option>
                            <option value="0<? echo $i; ?>:30"><? echo $i; ?>:30</option>   
                        <?}?>
                    </select>
                    <select name="before" id="before">
                        <? for ($i=9; $i < 22; $i++) {?>
                            <option value="<? echo $i; ?>:00"><? echo $i; ?>:00</option>
                            <option value="<? echo $i; ?>:30"><? echo $i; ?>:30</option>   
                        <?}?>
                    </select>
                    <p>Удобное время для звонка</p><br>
                    <span>с</span>
                    <span class="after" data-on='0'>
                        <b>9:00</b>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                        <ul>
                        <? for ($i=9; $i < 21; $i++) {?>
                            <li data-value="<? echo $i; ?>:00"><? echo $i; ?>:00</li>
                            <li data-value="<? echo $i; ?>:30"><? echo $i; ?>:30</li>   
                        <?}?>
                        </ul>
                    </span> 
                    <span>по</span>
                    <span class="before" data-on='0'>
                        <b>9:30</b>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                        <ul>
                        <? for ($i=9; $i < 22; $i++) {?>
                            <li data-value="<? echo $i; ?>:00"><? echo $i; ?>:00</li>
                            <li data-value="<? echo $i; ?>:30"><? echo $i; ?>:30</li>   
                        <?}?>
                        </ul>
                    </span>
                </div>
                <button type="submit">Отправить обращение<i class="fa fa-caret-right" aria-hidden="true"></i></button>
            </form>
        </section>
        <? if($_SERVER['SERVER_NAME'] == 'victoria.local') {?>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
        <? } ?>
        <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
        <script type="text/javascript" src="js/autosize.min.js"></script>
        <script type="text/javascript" src="js/common.js"></script>
    </div>
    <? if($_SERVER['SERVER_NAME'] !== 'victoria.local') {
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
    } 
    ?>