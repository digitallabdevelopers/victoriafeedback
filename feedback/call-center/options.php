<?php
  require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
  CModule::IncludeModule("iblock");

  function getListFromInfoblock($iblockId) {
    $res = CIBlockElement::GetList(array("sort"=>"asc", "name"=>"asc"), array("IBLOCK_ID"=> $iblockId));
    $list = array();
    while ($el = $res->GetNext()) {
      // Проперть с магазинами
      $list[(string) $el['ID']] = $el['NAME'];
    }

    return $list;
  }

	$options = array(
    'region' => array(
      'Москва',
      'Калининград'
    ),
    'type' => array(
      'Скидки и акции',
      'Действия сотрудников магазина',
      'Наличие и качество товаров',
      'Наличие очередей',
      'Работа сайта, мобильного приложения и инфокиосков',
      'Состояние магазина и прилегающей территории',
      'Подарочные карты',
      'Подписка и информационная рассылка',
      'Другое'
    ),
    'shop' => getListFromInfoblock(21) // Список магазинов
   );

  echo json_encode($options);
?>