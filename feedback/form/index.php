<?
    if($_SERVER['SERVER_NAME'] !== 'victoria.local') {
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
        $APPLICATION->SetTitle("Обратная связь");
    }
?>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!--meta name="viewport" content="width=1024" /-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Обратная связь</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link href="css/media.css" rel="stylesheet" type="text/css" />
    <? if($_SERVER['SERVER_NAME'] == 'victoria.local') {?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    <? } ?>
    <? if($_SERVER['SERVER_NAME'] !== 'victoria.local') {
        require($_SERVER["DOCUMENT_ROOT"]."/adaptive-menu/index.php");
    }?>
    <script>
    //if($(window).width() < 568){
    //    document.write('<link href="css/media.css" rel="stylesheet" type="text/css" /><link href="/adaptive-menu/css/style.css" rel="stylesheet" type="text/css" /><link href="/adaptive-menu/css/media.css" rel="stylesheet" type="text/css" />');
    //}
    </script>
    <link rel="stylesheet" type="text/css" href="../css/ui.css">
    <link rel="stylesheet" type="text/css" href="../css/fa.css">
    <script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
    <script src="../js/Icecream.js"></script>
    <script src="../js/canvas.js"></script>
   

<div class="crumbs main">
    <div class="right">
        <a id='step-1'href="/obratnaya-svyaz/">Выберите раздел</a> <span class="sep fa fa-caret-right"></span>
        <a id='step-2'class="active" href="#">Заполните форму</a> <span class="sep fa fa-caret-right"></span>
        <a id='step-3' href="#">Укажите контактные данные</a>
    </div>
</div>

<div class="main">
    <form id="form" action="../send.php" method="POST" enctype="multipart/form-data">
        <input type="hidden" value="<?= $_GET['id'];?>" name="formId" id="formId" />
<?php
    function forms($menu, $path = array(), $data = array())
    {
        foreach($menu->item as $sub)
        {
            $name = $path;
            $name[] = $sub->text;

            if($sub->fieldset)
                $data[] = array(
                    implode(" - ", $name),
                    $sub->fieldset
                );
            elseif($sub->menu)
                $data = forms($sub->menu, $name, $data);
        }

        return $data;
    }

    $tree = simplexml_load_file('../menu.xml' );
    $fs = forms($tree);

    $f = @$fs[ intval($_GET['id']) - 1];
    if(isset($f))
    {
        //echo "<h2>".$f[0]."</h2>";
        echo $f[1]->asXML();
?>
    <fieldset class="step-1">
        <label class="emotions">Выразите свои эмоции:</label>
        <input type="hidden" name="emotions" class="emotions">
        <div id="animation_container" style="background-color:rgba(255, 255, 255, 1.00); width:440px; height:395px">
            <canvas id="canvas" width="440" height="395" style="position: absolute; display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
            <div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:440px; height:395px; position: absolute; left: 0px; top: 0px; display: block;"></div>
        </div>
    </fieldset>
    <fieldset class="step-2">
        <!--h3>Чтобы мы связались с Вами <br>по результатам рассмотрения Вашего <br>обращения, укажите контактные данные</h3-->
        <input type="text" name='name' class="required" placeholder="Представьтесь, пожалуйста">
        <input type="text" class='phone' name='contact' placeholder="Телефон">
        <input type="text" name='email' placeholder="Электронный адрес">
        <input type="hidden" name='control'>
        <input type="checkbox" id="i-agree" name="needFeedback"> 
        <label class="checkbox" data-actived="off" for="i-agree">
            <span class="checkbox" data-actived='off' name="needFeedback"></span>
            <p>Я хочу чтобы со мной связались после обработки обращения</p>
        </label>
    </fieldset>
    <img class="card" src="../css/images/card.png">
    <img class="check" src="../css/images/check.png">
<?
    }
    else
    {
        echo '<h1>Форма не найдена</h1><br>';
    }
    echo "</div>";
?> 
<div class="controls">
<? if(isset($f)){ ?>
    <a href="/feedback.php" class="back"><span class="fa fa-caret-left"></span> Назад</a>
    <div class="step-1">
        <span class="on">Далее<i class="fa fa-caret-right"></i></span>
    </div>
<? }else{ ?>
    <a href="/feedback.php" class="back"><span class="fa fa-caret-left"></span> Назад</a>
<? } ?>
    <div class="step-2">
        <button type="submit">Отправить обращение<i class="fa fa-caret-right"></i></button>
    </div>
</div>
</form>
</div> 
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript" src="../js/jq.sb.js"></script>
<script type="text/javascript" src="../js/jquery.maskedinput.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
<script type="text/javascript" src="../js/url.min.js"></script>
<script type="text/javascript" src="../js/autosize.min.js"></script>
<script type="text/javascript" src="../js/init.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script>init();</script>
<?  if($_SERVER['SERVER_NAME'] !== 'victoria.local') {
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
    } 
?>
